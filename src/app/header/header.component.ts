import { Component, OnInit } from '@angular/core';

//Imported for login
import { MdDialog, MdDialogRef } from '@angular/material';
import { LoginComponent } from '../login/login.component';

@Component({
  selector: 'app-header',
  templateUrl: './header.component.html',
  styleUrls: ['./header.component.scss']
})
export class HeaderComponent implements OnInit {

	//include reference to MdDialog
  constructor(private dialog: MdDialog) { }

  ngOnInit() {
  }
  //For openLoginForm() in header template
  openLoginForm() {
  	this.dialog.open(LoginComponent, {width: '500px', height: '400px'})
  }

}
