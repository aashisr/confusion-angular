import { Component, OnInit, Inject } from '@angular/core';

import { Dish } from '../shared/dish';
import { DishService } from '../services/dish.service';
import { Promotion } from '../shared/promotion';
import { PromotionService } from '../services/promotion.service';
import { Leader } from '../shared/leader';
import { LeaderService } from '../services/leader.service';
import { flyInOut, expand } from '../animations/app.animation';

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.scss'],
  host: {
    '[@flyInOut]': 'true',
    'style': 'display: block'
  },
  animations: [
    flyInOut(),
    expand()
  ]
})
export class HomeComponent implements OnInit {
	//declare dish as type Dish and promotion as type Promotion
	featuredDish: Dish;
	featuredPromotion: Promotion;
  featuredLeader: Leader;
  dishErrMess: string;
  promotionErrMess: string;
  leaderErrMess: string;

	//Make services available to application 
  constructor(private dishservice: DishService,
  	private promotionservice: PromotionService,
    private leaderservice: LeaderService,
    @Inject('BaseURL') private BaseURL) { }

  //Fetch featured dishes
  ngOnInit() {
  	this.dishservice.getFeaturedDish()
      .subscribe(featuredDish => this.featuredDish = featuredDish,
        errMess => this.dishErrMess = <any>errMess);

  	this.promotionservice.getFeaturedPromotion()
      .subscribe(featuredPromotion => this.featuredPromotion = featuredPromotion,
        errMess => this.promotionErrMess = <any>errMess);

    this.leaderservice.getFeaturedLeader()
      .subscribe(featuredLeader => this.featuredLeader = featuredLeader,
        errMess => this.leaderErrMess = <any>errMess);
  } 

}
