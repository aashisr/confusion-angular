import { Injectable } from '@angular/core';
import { Dish } from '../shared/dish';
//import { DISHES } from '../shared/dishes';
import { Http, Response } from '@angular/http';
import { Observable } from 'rxjs/Observable';

//Download DISHES from server side
//import { baseURL } from '../shared/baseurl';
//Enables us to process the data we obtain from server side
import { ProcessHTTPMsgService } from './process-httpmsg.service';
import { RestangularModule, Restangular } from 'ngx-restangular';


import 'rxjs/add/operator/delay';
import 'rxjs/add/observable/of';
import 'rxjs/add/operator/map';
import 'rxjs/add/operator/catch';

@Injectable()
export class DishService {

  constructor(private restangular: Restangular,
    private processHTTPMsgService: ProcessHTTPMsgService) { }
  //getDishes method returns an observable as Dish array instead of Dish array
  getDishes(): Observable<Dish[]> {
    //return promise by resolving promise immediately
  	/*return Promise.resolve(DISHES); */

    //return promise by resolving promise after time delay
    /*
    return new Promise(resolve => {
      //Simulate server latency with 2 second delay
      setTimeout(() => resolve(DISHES), 2000);
    }); */

    // return observables
    //return Observable.of(DISHES).delay(2000);

    //return from Http get method
    /*
    return this.http.get(baseURL + 'dishes')
      .map(res => { return this.processHTTPMsgService.extractData(res); })
      .catch(error => { return this.processHTTPMsgService.handleError(error); });
      */

    //return by restangular method
    //this will access localhost:3000/dishes
    return this.restangular.all('dishes').getList();
  }  

  //Identify the dish with id which is a number
  //Or give me the dish with the id corresponding to the number
  getDish(id: number): Observable<Dish> {
  	//DISHES is an array
  	//Filter and return only the selected  element of array
  	//Arrow function is shorthand way of writing function
  	/*
    return new Promise(resolve => {
      setTimeout(() => resolve(DISHES.filter((dish) => (dish.id === id))[0]), 2000);
    });   */

    //Return observable
    //return Observable.of(DISHES.filter((dish) => (dish.id === id))[0]).delay(2000);

    //return from Http get method
    /*
    return this.http.get(baseURL + 'dishes/' + id)
      .map(res => { return this.processHTTPMsgService.extractData(res); })
      .catch(error => { return this.processHTTPMsgService.handleError(error); });
      */

    //return from restangular method
    return this.restangular.one('dishes', id).get();
  }

  //Returns the dish whose featured is set to true
  getFeaturedDish(): Observable<Dish> {
  	//It returns an array so [0] is used to return a single element
  	/*
    return new Promise(resolve => {
      setTimeout(() => resolve(DISHES.filter((dish) => (dish.featured))[0]), 2000);
    });  */

    //Return observable
    //return Observable.of(DISHES.filter((dish) => (dish.featured))[0]).delay(2000);

    //return from restangular method
    return this.restangular.all('dishes').getList({featured: true})
      .map(dishes => dishes[0]);
  }

  //Add new method to return all dish ids
  getDishIds(): Observable<number[] | any> {
    //map takes each item from the DISHES array and map that item into another item and construct another array and return that modified array
    //return Observable.of(DISHES.map(dish => dish.id)).delay(2000);

    //return from Http get method, same for restangular
    return this.getDishes()
      .map(dishes => { return dishes.map(dish => dish.id) })
      .catch(error => { return error; });
  }

}
