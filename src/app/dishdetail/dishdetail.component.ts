import { Component, OnInit, Inject } from '@angular/core';

import { Params, ActivatedRoute } from '@angular/router';
import { Location } from '@angular/common';

//For reactive form
import { FormBuilder, FormGroup, Validators } from '@angular/forms';

import { Dish } from '../shared/dish';
import { Comment } from '../shared/comment';
import { DishService } from '../services/dish.service';

import 'rxjs/add/operator/switchMap';

import { visibility, flyInOut, expand } from '../animations/app.animation';

@Component({
  selector: 'app-dishdetail',
  templateUrl: './dishdetail.component.html',
  styleUrls: ['./dishdetail.component.scss'],
  host: {
    '[@flyInOut]': 'true',
    'style': 'display: block'
  },
  animations: [
    flyInOut(),
    visibility(),
    expand()
  ]
})

export class DishdetailComponent implements OnInit {
  //store returned valuses in variable dish of type Dish
	dish: Dish;
  //Temporary variable
  dishcopy = null;
  dishIds: number[];
  prev: number;
  next: number;
  commentForm: FormGroup;
  comment: Comment;
  errMess: string;
  visibility = 'shown';
  //Create JS object for commentFormErrors
  commentFormErrors = {
    'author': '',
    'comment': ''
  };

  validationMessages = {
    'author': {
      'required': 'Author name is required.',
      'minlength': 'Author name must be at least 2 characters long'
    },

    'comment': {
      'required': 'Comment is required.'
    }
  };

  constructor(
    private dishservice: DishService,
  	private route: ActivatedRoute,
    private location: Location,
    private fb: FormBuilder,
    @Inject('BaseURL') private BaseURL
  ) { 
      this.createForm();
    }

  ngOnInit() {
    //To switch between different dishes using dishIds
    this.dishservice.getDishIds()
      .subscribe(dishIds => this.dishIds = dishIds);

    //Change following code to use observable
  	/*
    let id = +this.route.snapshot.params['id'];
    this.dishservice.getDish(id)
    .subscribe(dish => this.dish = dish);
    */
    this.route.params
    .switchMap((params: Params) => { this.visibility = 'hidden'; return this.dishservice.getDish(+params['id'])})
    .subscribe(dish => { this.dish = dish; this.dishcopy = dish; this.setPrevNext(dish.id); this.visibility = 'shown'; },
      errmess => this.errMess = <any>errmess);
  	
  }

  //Setting up Prev and Next for dishids
  setPrevNext(dishId: number) {
    let index = this.dishIds.indexOf(dishId);
    this.prev = this.dishIds[(this.dishIds.length + index - 1)%this.dishIds.length];
    this.next = this.dishIds[(this.dishIds.length + index + 1)%this.dishIds.length];
  }

  //To go back to the menu from dishdetail
  goBack(): void {
  	this.location.back();
  }

  //Creating a comment form, after this map it to the template
  createForm(): void {
    this.commentForm = this.fb.group({
      author: ['', [Validators.required, Validators.minLength(2)]],
      rating: 5,
      comment: ['', Validators.required]
    });

    //When form experiences change in any of its form elements, then angular framework provides valueChanges observable to trigger the form validation

    this.commentForm.valueChanges
      .subscribe(data => this.onValueChanged(data));
    this.onValueChanged();  //(re)set form validation messages now
  }

  //Define function onValueChanged
  onValueChanged(data?: any){
    if (!this.commentForm){ return; }
    //define a constant called form
    const form = this.commentForm;

    for (const field in this.commentFormErrors) {
      this.commentFormErrors[field] = '';
      const control = form.get(field);
      if (control && control.dirty && !control.valid) {
        const messages = this.validationMessages[field];
        for (const key in control.errors) {
          this.commentFormErrors[field] += messages[key] + '';
        }
      }
    }
  }

  onSubmit() {
    this.comment = this.commentForm.value;
    this.comment.date = new Date().toISOString();
    console.log(this.comment);
    //comments here might be comments from db.json
    this.dishcopy.comments.push(this.comment);
    this.dishcopy.save()
      .subscribe(dish => { this.dish = dish; console.log(this.dish); });
    this.commentForm.reset({
      author: '',
      rating: 5,
      comment: ''
    });
  }

}
