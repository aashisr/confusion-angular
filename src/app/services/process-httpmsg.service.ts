import { Injectable } from '@angular/core';

import { Observable } from 'rxjs/Observable';
import { Http, Response } from '@angular/http';

import 'rxjs/add/observable/throw'; //helps us to throw an error whenever it arises

@Injectable()
export class ProcessHTTPMsgService {

  constructor() { }

  //Include new method called extractData
  //extractData takes a parameter Response from server side and process this response
  //and extract data from body of the message
  public extractData(res: Response) {
  	let body = res.json();

  	//return body
  	//if body is null return an empty object
  	console.log(body);
  	return body || {};
  }

  public handleError(error: Response | any) {
     // In a real world app, you might use a remote logging infrastructure
    let errMsg: string;

    if (error instanceof Response) {  //Error arises from server side
      const body = error.json() || '';  // || is or
      const err = body.error || JSON.stringify(body);
      errMsg = `${error.status} - ${error.statusText || ''} ${err}`;

    } else {
      errMsg = error.message ? error.message : error.toString();
    }

    //Return as an observable with throw method
    return Observable.throw(errMsg);
  }

}

/* What we need in this sfile is the way of extracting the body from the
 response message and returning that body in the form of a json string which 
 can be mapped into a JavaScript Object */