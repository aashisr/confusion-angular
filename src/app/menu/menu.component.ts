import { Component, OnInit, Inject } from '@angular/core';

import { Dish } from '../shared/dish';

import { DishService } from '../services/dish.service';
// Importing all the DISHES from dishes folder

import { flyInOut, expand } from '../animations/app.animation';

@Component({
  selector: 'app-menu',
  templateUrl: './menu.component.html',
  styleUrls: ['./menu.component.scss'],
  host: {
    '[@flyInOut]': 'true',
    'style': 'display: block'
  },
  animations: [
    flyInOut(),
    expand()
  ]
})
export class MenuComponent implements OnInit {

	dishes: Dish[];
  errMess: string;
  /* No need of this because of Inject
	selectedDish: Dish;
  */

  //Creates dishService object and make it available to menu component
  constructor(private dishService: DishService,
    @Inject('BaseURL') private BaseURL ) { }

  //When component is created, this method
  // is going to be executed
  ngOnInit() {
    this.dishService.getDishes()  //Fetch dishes from dishService
      // .then(dishes => this.dishes = dishes ); //Implemented using Promise
      .subscribe(dishes => this.dishes = dishes,  
      //Error messages also can be handled by subscribe
        errmess => this.errMess = <any>errmess); //Implemented using Observable
  }

  /* Not needed
  onSelect(dish: Dish){
  	this.selectedDish = dish;
  }
  */
}
