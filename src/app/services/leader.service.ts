import { Injectable } from '@angular/core';

import { Leader } from '../shared/leader';
import { LEADERS } from '../shared/leaders';

import { Observable } from 'rxjs/Observable';
import { RestangularModule, Restangular } from 'ngx-restangular';

import 'rxjs/add/operator/toPromise';
import 'rxjs/add/operator/delay';
import 'rxjs/add/observable/of';

@Injectable()
export class LeaderService {

  constructor(private restangular: Restangular) { }

  //Get details of all leaders
  getLeaders(): Observable<Leader[]> {
  	//return Observable.of(LEADERS).delay(2000);
    return this.restangular.all('leaders').getList();
  }

  //Get details of a specific leader
  getLeader(id: number): Observable<Leader> {
  	//return Observable.of(LEADERS.filter((leader) => (leader.id === id))[0]).delay(2000);
    return this.restangular.one('leaders', id).get();
  }

  //Get details of a featured leader
  getFeaturedLeader(): Observable<Leader> {
    //return Observable.of(LEADERS.filter((leader) => leader.featured)[0]).delay(2000);
    return this.restangular.all('leaders').getList({featured: true})
      .map(leaders => leaders[0]);
  }

}
